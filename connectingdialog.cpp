#include "connectingdialog.h"
#include "ui_connectingdialog.h"

ConnectingDialog::ConnectingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectingDialog)
{
    ui->setupUi(this);
    ui->nickName->setFocus();
}

ConnectingDialog::~ConnectingDialog()
{
    delete ui;
}

void ConnectingDialog::setParams()
{
    if (ui->nickName->text().isEmpty()) {
        QMessageBox::critical(this,
                              QString::fromUtf8("Не заполнены поля"),
                              QString::fromUtf8("Введите Nickname"));
    } else {
        nickName.clear();
        nickName = ui->nickName->text();
        ipAddress.clear();
        ipAddress.append(ui->spinBoxIP1->text());
        ipAddress.append(QString("."));
        ipAddress.append(ui->spinBoxIP2->text());
        ipAddress.append(QString("."));
        ipAddress.append(ui->spinBoxIP3->text());
        ipAddress.append(QString("."));
        ipAddress.append(ui->spinBoxIP4->text());
        this->done(1);
    }
}

QString ConnectingDialog::getIPaddress()
{
    return ipAddress;
}

QString ConnectingDialog::getNickname()
{
    return nickName;
}
