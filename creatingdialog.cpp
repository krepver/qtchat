#include "creatingdialog.h"
#include "ui_creatingdialog.h"

CreatingDialog::CreatingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreatingDialog)
{
    ui->setupUi(this);
    ui->nickName->setFocus();
}

CreatingDialog::~CreatingDialog()
{
    delete ui;
}

void CreatingDialog::setParams()
{
    if (ui->nickName->text().isEmpty()) {
        QMessageBox::critical(this,
                              QString::fromUtf8("Не заполнены поля"),
                              QString::fromUtf8("Введите Nickname"));
    } else {
       m_nickName = ui->nickName->text();
       this->done(1);
    }
}


QString CreatingDialog::getNickname()
{
    return m_nickName;
}
