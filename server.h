#ifndef SERVER_H
#define SERVER_H

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QMessageBox>
#include <QTextEdit>
#include <QStringList>

#include "serverclient.h"
#include "mainwindow_interface.h"
#include "func_def.h"

using namespace std;

class Server: public QObject
{
    Q_OBJECT
public:
    Server(MainWindowInterface* newMainWindow, WindowInterface* logWindow,QObject* parent = 0);
public slots:
    void start();
    void newConnection();
    void clientError(QAbstractSocket::SocketError error);
    void clientReadyRead();
    void clientDisconnected();
private:
    SERVER_COMAND strToCommand(QString cmd);
    bool loginClient(ServerClient* client, QString nickName);

    QTcpServer* m_TcpServer;
    QMap<QTcpSocket*, ServerClient*> m_Clients;
    MainWindowInterface* m_mainWindow;
    WindowInterface* m_logWindow;
    QStringList m_listNicks;

    static const quint16 m_port = 26266;
};

#endif // SERVER_H
