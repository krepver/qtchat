#-------------------------------------------------
#
# Project created by QtCreator 2012-10-25T13:31:08
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtChat
TEMPLATE = app


SOURCES += main.cpp\
        loginwindow.cpp \
        mainwindow.cpp \
    connectingdialog.cpp \
    creatingdialog.cpp \
    func_def.cpp \
    server.cpp \
    serverclient.cpp \
    serverlog.cpp

HEADERS  += loginwindow.h \
        mainwindow.h \
    connectingdialog.h \
    creatingdialog.h \
    func_def.h \
    server.h \
    serverclient.h \
    mainwindow_interface.h \
    serverlog.h \
    window_interface.h

FORMS    += loginwindow.ui \
        mainwindow.ui \
    connectingdialog.ui \
    creatingdialog.ui \
    serverlog.ui
