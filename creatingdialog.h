#ifndef CREATINGDIALOG_H
#define CREATINGDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "func_def.h"

namespace Ui {
class CreatingDialog;
}

class CreatingDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CreatingDialog(QWidget *parent = 0);
    ~CreatingDialog();
    QString getNickname();

public slots:
    void setParams();

private:
    Ui::CreatingDialog *ui;
    QString m_nickName;

};

#endif // CREATINGDIALOG_H
