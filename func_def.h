#ifndef FUNC_DEF_H
#define FUNC_DEF_H

#include <QString>
#include <QByteArray>

#define _secretKey (quint32)15

enum USER_REPLY {
    USER_REPLY_UNDEFINED = 0,
    USER_REPLY_CLIENT = 1,
    USER_REPLY_SERVER = 2,
    USER_REPLY_CLOSE = 3
};

enum SERVER_COMAND {
    SERVER_COMAND_UNDEFINED = 0,
    SERVER_COMAND_LOGIN = 1
};

class EncodeDecodeStr {
public:
    static QString encodeStr(const QString& str);

    static QString decodeStr(const QString &str);
protected:
    EncodeDecodeStr() {};

};


#endif // FUNC_DEF_H
