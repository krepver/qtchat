#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QDialog>
#include "func_def.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

public slots:
    void connectToServer();
    void startServer();

    
private:
    Ui::LoginWindow *ui;
};

#endif // LOGINWINDOW_H
