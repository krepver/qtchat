#include "server.h"

Server::Server(MainWindowInterface* mainWindow, WindowInterface* logWindow ,QObject *parent): QObject(parent)
{
    m_mainWindow = mainWindow;
    m_logWindow = logWindow;
    m_TcpServer = NULL;
    m_listNicks.clear();
}

void Server::start()
{
    m_TcpServer = new QTcpServer(this);
    connect(m_TcpServer, SIGNAL(newConnection()), this, SLOT(newConnection()));
    if (!m_TcpServer->listen(QHostAddress::Any, m_port))  {
        m_logWindow->printMessage(QString("Failed creating server:"));
        m_logWindow->printMessage(m_TcpServer->errorString());
    } else {
        QString prString;
        prString += "Successfully created server. Waiting connections on port ";
        prString += QString::number(m_port);
        prString += "...";
        m_logWindow->printMessage(prString);
    }
}

void Server::newConnection()
{
    QTcpSocket* connection = m_TcpServer->nextPendingConnection();

    if(connection)
    {
        ServerClient* client = new ServerClient();

        connect(connection, SIGNAL(error(QAbstractSocket::SocketError)),
                this, SLOT(clientError(QAbstractSocket::SocketError)));
        connect(connection, SIGNAL(readyRead()), this, SLOT(clientReadyRead()));
        connect(connection, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));

        m_Clients[connection] = client;

        QString prString;
        prString += "\nClient connected!\nClient IP: ";
        prString += connection->peerAddress().toString();
        prString += "\nClient port: ";
        prString += QString::number(connection->peerPort());
        prString += "\nSocket descriptor: ";
        prString += QString::number(connection->socketDescriptor());

        m_logWindow->printMessage(prString);


    }
}

void Server::clientError(QAbstractSocket::SocketError error)
{
    QTcpSocket* clientSocket = (QTcpSocket*)sender();

    QString clientInfo;

    clientInfo.append("\nClient error!");
    clientInfo.append("\nClient IP: ");
    clientInfo.append(clientSocket->peerAddress().toString());
    clientInfo.append("\nError number: ");
    clientInfo.append(QString::number(error));
    clientInfo.append("\nError text: ");
    clientInfo.append(clientSocket->errorString());

    m_logWindow->printMessage(clientInfo);

    delete m_Clients[clientSocket];
    m_Clients.remove(clientSocket);
}

void Server::clientReadyRead()
{
    QTcpSocket* clientSocket = (QTcpSocket*)sender();
    QStringList lines;
    while (clientSocket->canReadLine()) {
//        char buf[1024];
//            qint64 lineLength = clientSocket->readLine(buf, sizeof(buf));
//            if (lineLength != -1) {
//                lines.append(QString::fromUtf8(buf));
//                // the line is available in buf
//            }
        lines.append(clientSocket->readLine());
    }

    QStringList::iterator it;
    for (it = lines.begin(); it != lines.end(); ++it) {
        QString m_Response = *it;
        QString strCommand;
        int pos = m_Response.indexOf(":");
        if (pos > -1) {
            strCommand = m_Response.section(':', 0, 0);
            SERVER_COMAND command = this->strToCommand(strCommand);
            QString str_nickName;
            switch (command) {
            case SERVER_COMAND_LOGIN:
                str_nickName = m_Response.section(':', 1, 1);
                str_nickName.resize(str_nickName.size()-2);
                if (this->loginClient(m_Clients[clientSocket], str_nickName)) {
                    clientSocket->write((strCommand + "\n").toUtf8());
                }
                break;
            default:
                break;
            }

            m_Response.resize(m_Response.size()-2);

            m_logWindow->printMessage(m_Response);
        }
    }


}

void Server::clientDisconnected()
{
    QTcpSocket* clientSocket = (QTcpSocket*)sender();

    QString clientInfo;

    clientInfo.append("\nClient disconnected!");
    clientInfo.append("\nClient IP: ");
    clientInfo.append(clientSocket->peerAddress().toString());

    m_logWindow->printMessage(clientInfo);

    delete m_Clients[clientSocket];
    m_Clients.remove(clientSocket);
}

SERVER_COMAND Server::strToCommand(QString cmd)
{
    SERVER_COMAND ret = SERVER_COMAND_UNDEFINED;
    if (cmd == QString("LOGIN")) {
        ret = SERVER_COMAND_LOGIN;
    }
    return ret;
}

bool Server::loginClient(ServerClient *client, QString newNickName)
{
    bool ret= false;
    if (m_listNicks.contains(newNickName)) {
        ret = false;
    } else {
        client->setNickName(newNickName);
        m_listNicks.append(newNickName);
        m_mainWindow->addUser(newNickName);
        ret = true;
    }
    return ret;
}
