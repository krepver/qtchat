#ifndef CONNECTINGDIALOG_H
#define CONNECTINGDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "func_def.h"

namespace Ui {
class ConnectingDialog;
}

class ConnectingDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ConnectingDialog(QWidget *parent = 0);
    ~ConnectingDialog();
    QString getIPaddress();
    QString getNickname();

public slots:
    void setParams();

private:
    Ui::ConnectingDialog *ui;
    QString nickName;
    QString ipAddress;
};

#endif // CONNECTINGDIALOG_H
