#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QThread>
#include <QMutex>
#include <QCloseEvent>

#include "loginwindow.h"
#include "connectingdialog.h"
#include "creatingdialog.h"
#include "func_def.h"
#include "server.h"
#include "mainwindow_interface.h"
#include "serverlog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public MainWindowInterface
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    USER_REPLY askUser();
    bool createServer();
    bool connectToServer();
    void printMessage(QString str);
    void addUser(QString str);
    void delUser(QString str);

    Ui::MainWindow *ui;

signals:
    void print();
    
public slots:

protected slots:
    void closeEvent(QCloseEvent *p_event);

private slots:
    void sendMessage();
    void pr();

private:
    USER_REPLY clientOrServer();
    bool clientDialog();
    bool serverDialog();

    QString m_ip;
    QString m_Nickname;
    Server* m_Server;
    QThread* m_ServerThread;
    QStringList m_printStrings;
    QMutex m_mutex;
    ServerLog* m_serverLog;
};

#endif // MAINWINDOW_H
