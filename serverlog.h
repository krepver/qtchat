#ifndef SERVERLOG_H
#define SERVERLOG_H

#include <QWidget>
#include <QMutex>
#include <QCloseEvent>

#include <window_interface.h>

namespace Ui {
class ServerLog;
}

class ServerLog : public QWidget, public WindowInterface
{
    Q_OBJECT
    
public:
    explicit ServerLog(QWidget *parent = 0);
    ~ServerLog();
    void printMessage(QString str);

    bool bl_canClose;
signals:
    void print();

private slots:
    void pr();

protected slots:
    void closeEvent(QCloseEvent *p_event);

private:
    Ui::ServerLog *ui;
    QStringList m_printStrings;
    QMutex m_mutex;
};

#endif // SERVERLOG_H
