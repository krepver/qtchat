#include "func_def.h"

QString EncodeDecodeStr::encodeStr(const QString& str)
{
    QByteArray arr(str.toUtf8());
    for(int i =0; i<arr.size(); i++)
        arr[i] = arr[i] ^ _secretKey;

    return QString::fromAscii(arr.toBase64());
}

QString EncodeDecodeStr::decodeStr(const QString &str)
{
    QByteArray arr = QByteArray::fromBase64(str.toAscii());
    for(int i =0; i<arr.size(); i++)
        arr[i] =arr[i] ^ _secretKey;

    return QString::fromUtf8(arr);
}
