#ifndef MAINWINDOW_INTERFACE_H
#define MAINWINDOW_INTERFACE_H
#include <QString>

#include "window_interface.h"

class MainWindowInterface {
public:
    virtual void addUser(QString str) = 0;
    virtual void delUser(QString str) = 0;
};



#endif // MAINWINDOW_INTERFACE_H
