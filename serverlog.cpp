#include "serverlog.h"
#include "ui_serverlog.h"

ServerLog::ServerLog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServerLog)
{
    ui->setupUi(this);
    connect(this, SIGNAL(print()), this, SLOT(pr()),Qt::QueuedConnection);
    bl_canClose = false;

}

ServerLog::~ServerLog()
{
    delete ui;
}


void ServerLog::printMessage(QString str)
{
    m_mutex.lock();
    m_printStrings.append(str);
    emit print();
    m_mutex.unlock();
}

void ServerLog::pr()
{
    m_mutex.lock();

    QStringList::iterator it;
    for (it = m_printStrings.begin(); it != m_printStrings.end(); ++it) {
        QString strToPrint = *it;
        if (!strToPrint.isEmpty()) {
            ui->plainTextEdit->appendPlainText(strToPrint);
            QTextCursor c = ui->plainTextEdit->textCursor();
            c.movePosition(QTextCursor::End);
            ui->plainTextEdit->setTextCursor(c);
        }
    }
    m_printStrings.clear();

    m_mutex.unlock();
}

void ServerLog::closeEvent(QCloseEvent *p_event)
{
    if (bl_canClose) {
        p_event->accept();
    } else {
        p_event->ignore();
    };
};
