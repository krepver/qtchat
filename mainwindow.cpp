#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(this, SIGNAL(print()), this, SLOT(pr()),Qt::UniqueConnection);

    ui->lineEdit->setFocus();

    m_Server = NULL;
    m_ServerThread = NULL;
    m_serverLog = NULL;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_serverLog;
}

void MainWindow::sendMessage()
{
    if (!ui->lineEdit->text().isEmpty()) {
        ui->textEdit->append("");

        QString message, q1;
        message.append("<p>");
        message.append(m_Nickname);
        message.append(": ");
        q1 = ui->lineEdit->text();
        ui->lineEdit->clear();
        message.append(q1);
        message.append("</p>");
        ui->textEdit->append(message);
        message.clear();

        ui->lineEdit->setFocus();

        message.append("<p>");
        message.append(m_Nickname);
        message.append(": ");
        q1 = EncodeDecodeStr::encodeStr(q1);
        message.append("<b>");
        message.append(q1);
        message.append("</b>");
        message.append("</p>");
        ui->textEdit->append(message);
        message.clear();

        message.append("<p>");
        message.append(m_Nickname);
        message.append(": ");
        q1 = EncodeDecodeStr::decodeStr(q1);
        message.append(q1);
        message.append("</p>");
        ui->textEdit->append(message);
        message.clear();
    }
}

USER_REPLY MainWindow::askUser()
{
    USER_REPLY retCode = USER_REPLY_UNDEFINED;
    retCode = clientOrServer();
    switch (retCode) {
        case USER_REPLY_CLIENT:
            if (!clientDialog()) retCode = USER_REPLY_UNDEFINED;
            break;
        case USER_REPLY_SERVER:
            if (!serverDialog()) retCode = USER_REPLY_UNDEFINED;
            break;
        default:
            this->deleteLater();
            retCode = USER_REPLY_CLOSE;
            break;
    }

    return retCode;
} 

bool MainWindow::createServer()
{
    m_serverLog = new ServerLog();
    m_serverLog->show();
    m_serverLog->printMessage(QString("Creating server..."));
    m_Server = new Server(this ,m_serverLog);
    m_ServerThread = new QThread(this);
    m_Server->moveToThread(m_ServerThread);
    connect(m_ServerThread, SIGNAL(started()), m_Server, SLOT(start()));
    m_ServerThread->start();
    m_ip = QString("127.0.0.1");
    this->connectToServer();
    return true;
}

bool MainWindow::connectToServer()
{
    ui->textEdit->append(QString("Connecting to server..."));
    return true;
}

USER_REPLY MainWindow::clientOrServer()
{
    LoginWindow* loginWindow;
    loginWindow = new LoginWindow(this);
    loginWindow->show();
    USER_REPLY loginResult = USER_REPLY_UNDEFINED;
    loginResult = (USER_REPLY)loginWindow->exec();
    loginWindow->close();
    loginWindow->deleteLater();
    return loginResult;
}

bool MainWindow::clientDialog()
{
    bool ret = false;
    ConnectingDialog* connectingDialog;
    connectingDialog = new ConnectingDialog(this);
    int connectingResult = 0;
    connectingDialog->show();
    connectingResult = connectingDialog->exec();
    if (!connectingResult) {
        ret = false;
    } else {
        m_ip = connectingDialog->getIPaddress();
        m_Nickname = connectingDialog->getNickname();
//        ui->listWidget->addItem(m_Nickname);
        ret = true;
    }
    connectingDialog->close();
    connectingDialog->deleteLater();
    return ret;
}

bool MainWindow::serverDialog()
{
    bool ret = false;
    CreatingDialog* creatingDialog;
    creatingDialog = new CreatingDialog(this);
    int connectingResult = 0;
    creatingDialog->show();
    connectingResult = creatingDialog->exec();
    if (!connectingResult) {
        ret = false;
    } else {
        m_Nickname = creatingDialog->getNickname();
//        ui->listWidget->addItem(m_Nickname);
        ret = true;
    }
    creatingDialog->close();
    creatingDialog->deleteLater();
    return ret;
}

void MainWindow::printMessage(QString str)
{
    m_mutex.lock();
    m_printStrings.append(str);
    emit print();
    m_mutex.unlock();
}

void MainWindow::pr()
{
    m_mutex.lock();

    QStringList::iterator it;
    for (it = m_printStrings.begin(); it != m_printStrings.end(); ++it) {
        QString strToPrint = *it;
        ui->textEdit->append(strToPrint);
        QTextCursor c = ui->textEdit->textCursor();
        c.movePosition(QTextCursor::End);
        ui->textEdit->setTextCursor(c);
    }
    m_printStrings.clear();

    m_mutex.unlock();
}

void MainWindow::closeEvent(QCloseEvent *p_event)
{
    m_serverLog->bl_canClose = true;
    m_serverLog->close();
    p_event->accept();
};

void MainWindow::addUser(QString str)
{
    ui->listWidget->addItem(str);
    QString msg;
    msg += "User ";
    msg += str;
    msg += " logged in.";
    this->printMessage(msg);
}

void MainWindow::delUser(QString str)
{
    QList<QListWidgetItem*> searchNicks;
    searchNicks = ui->listWidget->findItems(str, Qt::MatchCaseSensitive);
//    QList<QListWidgetItem*>::const_iterator it;
    QList<QListWidgetItem*>::iterator it;
    for (it = searchNicks.begin(); it != searchNicks.end(); ++it) {
        QListWidgetItem* item = *it;
        ui->listWidget->removeItemWidget(item);
        QString msg;
        msg += "User ";
        msg += str;
        msg += " logged out.";
        this->printMessage(msg);
    }
}
