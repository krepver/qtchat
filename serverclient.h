#ifndef SERVERCLIENT_H
#define SERVERCLIENT_H

#include <QString>
#include <QMutex>

class ServerClient
{
public:
    ServerClient();
    bool isAuthorized();
    void setNickName(QString newNickName);

private:
    QString m_nickName;
    QMutex m_mutex;
    bool is_loggedIn;
    bool is_authorized;
};

#endif // SERVERCLIENT_H
