#ifndef WINDOWINTERFACE_H
#define WINDOWINTERFACE_H
#include <QString>

class WindowInterface {
public:
    virtual void printMessage(QString str) = 0;
};
#endif // WINDOWINTERFACE_H
