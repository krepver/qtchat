#include "serverclient.h"

ServerClient::ServerClient()
{
    m_nickName = "";
    is_loggedIn = false;
    is_authorized = false;
}

void ServerClient::setNickName(QString newNickName)
{
    m_mutex.lock();
    m_nickName = newNickName;
    m_mutex.unlock();
}

