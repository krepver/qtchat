#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow* w;
    w = new MainWindow();
    USER_REPLY userAnswer = USER_REPLY_UNDEFINED;
    while (userAnswer == USER_REPLY_UNDEFINED) {
        userAnswer = w->askUser();
        if (userAnswer == USER_REPLY_CLIENT ) {
            w->connectToServer();
        } else if (userAnswer == USER_REPLY_SERVER) {
            w->createServer();
        }
    }
    w->show();
    
    return a.exec();
}
