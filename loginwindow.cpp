#include "loginwindow.h"
#include "ui_loginwindow.h"

LoginWindow::LoginWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::startServer()
{
    this->done(USER_REPLY_SERVER);
}

void LoginWindow::connectToServer()
{
    this->done(USER_REPLY_CLIENT);
}
